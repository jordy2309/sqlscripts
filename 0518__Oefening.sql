USE ModernWays;
ALTER TABLE Huisdieren ADD COLUMN Geluid VARCHAR(20);
SELECT * FROM huisdieren;
SET SQL_SAFE_UPDATES = 0;
UPDATE huisdieren SET Geluid = "WAF!" WHERE Soort = "hond";
UPDATE huisdieren SET Geluid = "miauwww..." WHERE Soort = "kat"; 
SET SQL_SAFE_UPDATES = 1;