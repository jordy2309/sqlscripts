USE ModernWays;
CREATE TABLE Metingen(
Tijdstip DATETIME not null,
Grootte SMALLINT not null,
Marge FLOAT(3,2) not null
);