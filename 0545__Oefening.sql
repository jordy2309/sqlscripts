USE ModernWays;
SELECT Artiest , SUM(Aantalbeluisteringen) AS "Aantal beluisteringen" 
FROM Liedjes 
WHERE LENGTH(Artiest) > 10 
GROUP BY Artiest 
HAVING SUM(Aantalbeluisteringen) >= 100;